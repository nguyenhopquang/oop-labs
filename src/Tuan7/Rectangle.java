/**
 * Class Rectangele ke thua tu class Shape
 * @author Nguyen Hop Quang
 * @version 1.0
 */
public class Rectangle extends  Shape{
    private int width = 1;
    private int length = 1;
    private int x;
    private int y;

    /**
     * Constructor rong
     */
    public Rectangle() {}

    /**
     * Constructor co tham so
     * @param w la width
     * @param l la length
     */
    public Rectangle(int w, int l) {
        width = w;
        length = l;
    }

    /**
     * Hàm khởi tạo của class Rectangle
     * @param w
     * @param l
     * @param cl
     * @param f
     */
    public Rectangle(int w, int l, String cl, boolean f, int x, int y) {
        super(cl, f, x, y);
        setWidth(w);
        setLength(l);
    }

    /**
     * Getter/Setter
     */
    public int getWidthR() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getArea() {
        return width*length;
    }

    public int getPerimeter() {
        return (width + length) *2;
    }

    /**
     * Hàm in ra thông tin
     * @return trả về màu sắc và isfilled và chiều dài và chiều rộng
     */
    @Override
    public String toString() {
        return "Shape: Rectangel\n" + "Color: "+ getColor() + "\nWidth: "  + getWidthR() + "\nLengh: " +getLength() + "\n";
    }

    /**
     * Xet 2 object
     * @param obj la object can xet
     * @return tra ve true false
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(getClass() != obj.getClass()) return false;
        else {
            Object other_rectangel = (Rectangle)obj;
            return (((Rectangle) other_rectangel).getWidthR() == width && ((Rectangle) other_rectangel).getLength() == length && ((Rectangle) other_rectangel).getPos1() == getPos1() && ((Rectangle) other_rectangel).getPos2() == getPos2());
        }
    }
}