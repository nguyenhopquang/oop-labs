/**
 * Class Circke ke thua tu class Shape
 * @author Nguyen Hop Quang
 * @version 1.0
 */
public class Circle extends Shape{
    private double radius = 1.0;
    final double PI = 3.141592;

    /**
     * Constructor rong
     */
    public  Circle() {}

    /**
     * Hàm khởi tạo của class Circle
     * @param ra biến tham số đầu vào
     */
    public  Circle(double ra) {

        radius = ra;
    }

    /**
     * Hàm khởi tạo của class Circle với 2 tham số đầu vào
     * @param ra
     * @param cl
     * @param f
     */
    public  Circle(double ra, String cl, boolean f, int x, int y) {
        super(cl,f, x, y);
        setRadius(ra);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * PI * radius;
    }

    /**
     * Hàm in ra thông tin
     * @return trả về màu sắc và isfilled và radius
     */
    @Override
    public String toString() {
        return "Shape: Circle \nRadius:" + getRadius() + "\nColor: " + getColor() + "\nIsFilled: " + isFilled() + "\n";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(getClass() != obj.getClass()) return false;
        else {
            Object other_circle = (Circle)obj;
            return (((Circle) other_circle).getX() == getX() && ((Circle) other_circle).getY() == getY() && ((Circle) other_circle).getRadius() == radius);
        }
    }
}