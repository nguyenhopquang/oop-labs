/**
 * Class  Triangleke thua tu class Shape
 * @author Nguyen Huu Hoa
 * @version 1.0
 * @since 2018-09-20
 */
public class Triangle extends Shape{
    private int side1, side2, side3;

    /**
     * Constructor rong
     */
    public Triangle() {

    }

    /**
     * Constructor co tham so
     * @param side1 la canh 1
     * @param side2 la canh 2
     * @param side3 la canh 3
     */
    public Triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    /**
     * Khỏi tạo contructor Triangle với nhiều tham sô
     *
     * @param sde1 la canh 1
     * @param sde2 la cnah 2
     * @param sde3 la canh 3
     * @param x la toa do
     * @param y la toa do
     * @param filled la filled
     * @param color la mau
     */
    public Triangle(int sde1, int sde2, int sde3, int x, int y, boolean filled, String color) {
            super(color, filled, x, y);
            side1 = sde1;
            side2 = sde2;
            side3 = sde3;
    }

    /**
     * Getter/Setter
     */
    public int getSide1() {
        return side1;
    }

    public void setSide1(int side1) {
        this.side1 = side1;
    }

    public int getSide2() {
        return side2;
    }

    public void setSide2(int side2) {
        this.side2 = side2;
    }

    public int getSide3() {
        return side3;
    }

    public void setSide3(int side3) {
        this.side3 = side3;
    }

    /**
     * Hàm trả về thông tin
     * @return
     */
    @Override
    public String toString() {
        return "Shape: Triangle\n" + "Side1:" + getSide1() + "\nSide2: " + getSide2() + "\nSide3: "  + getSide3() + "\nColor: " + getColor() + "" +
                "\nIsFilled: " + isFilled() +"\n";
    }

    /**
     * Hàm kiểm tra xem một đầu vào có trungf với Object không
     * @param obj là đối tượng đầu vào
     * @return kết quả true hoặc false
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(getClass() != obj.getClass()) return false;
        else {
            Object other_triangle = (Triangle)obj;
            return (((Triangle) other_triangle).getSide1() == getSide1() && ((Triangle) other_triangle).getSide2() == getSide2() && ((Triangle) other_triangle).getSide3() == getSide3() && ((Triangle) other_triangle).getPos1() == getPos1() && ((Triangle) other_triangle).getPos2() == getPos2());
        }
    }
}
