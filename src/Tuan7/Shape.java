/**
 * Class Shape la lop dan suat
 * @author Nguyen Hop Quang
 * @version 1.0
 */
public class Shape extends Layer{
    private String color = new String("red");
    private boolean filled = true;
    private int x, y;

    /**
     * Construtor rong
     */
    public  Shape() {}

    /**
     * Getter/Setter
     */
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /**
     * Khởi tạo constructor Shape với 2 tham số đầu vào
     * @param cl
     * @param f
     */
    public  Shape(String cl, boolean f, int x, int y) {
        color = cl;
        filled = f;
        setPos1(x);
        setPos2(y);
    }

    /**
     * Hàm in ra thông tin
     * @return trả về màu sắc và isfilled
     */
    public String toString() {
        return "Color: "+ getColor() + "\nIsFilled: " + isFilled() + "\n";
    }
}