/**
 * Chương trình kiểm thử các phương thức
 * @author Nguyen Hop Quang
 * @version 1.0
 */
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("Khoi tao cac Shape: ");
        Shape shape1 = new Rectangle(300, 400, "red", true, 3, 4);
        Shape shape2 = new Rectangle(300, 400, "red", true, 3, 4);
        Shape shape3 = new Rectangle(300, 400, "red", true, 3, 4);
        Shape shape4 = new Circle(3.14, "green", false, 5, 6);
        Shape shape5 = new Circle(3.14, "blue", true, 5, 6);
        Shape shape6 = new Circle(3.14, "red", true, 5, 6);
        Shape shape7 = new Square( 5, "blue", true, 7, 8);
        Shape shape8 = new Square( 5, "blue", true, 7, 8);
        Shape shape9 = new Square( 5, "blue", true, 7, 8);
        Shape shape10 = new Triangle(3, 4, 5, 6, 7, true, "green");
        Shape shape11 = new Triangle(3, 4, 5, 6, 7, true, "green");
        Shape shape12= new Triangle(3, 4, 5, 6, 7, true, "green");
        Shape shape13 = new Hexagon(1, 2, 3, 4, 5, 6, 7, 8, "yellow", true);
        Shape shape14 = new Hexagon(1, 2, 3, 4, 5, 6, 7, 8, "yellow", true);
        Shape shape15 = new Hexagon(1, 2, 3, 4, 5, 6, 7, 8, "yellow", true);

        /**
         * Tạo ra các listShape
         */
        ArrayList<Shape> listShape1 = new ArrayList<Shape>();
        ArrayList<Shape> listShape2 = new ArrayList<Shape>();
        ArrayList<Shape> listShape3 = new ArrayList<Shape>();
        ArrayList<Shape> listShape4 = new ArrayList<Shape>();
        ArrayList<Shape> listShape5 = new ArrayList<Shape>();

        /**
         * Add các shape Rectangle vào listShape1
         */
        listShape1.add(shape1);
        listShape1.add(shape2);
        listShape1.add(shape3);
        /**
         * Add các shape  Circle vào listShape2
         */
        listShape2.add(shape4);
        listShape2.add(shape5);
        listShape2.add(shape6);
        /**
         * Add các shape Square vào listShape3
         */
        listShape3.add(shape7);
        listShape3.add(shape8);
        listShape3.add(shape9);
        /**
         * Add các shape Triangle vào listShape4
         */
        listShape4.add(shape10);
        listShape4.add(shape11);
        listShape4.add(shape12);
        /**
         * Add các shape Hexagon vào listShape5
         */
        listShape5.add(shape13);
        listShape5.add(shape14);
        listShape5.add(shape15);

        /**
         * Khởi tạo các layer, thêm listshape vào layer
         */
        Layer layer1 = new Layer(listShape1, 300, 400, 3, 4, true);
        Layer layer2 = new Layer(listShape2, 200, 300, 1, 2, false);
        Layer layer3 = new Layer(listShape3, 500, 500, 5, 6, true);
        Layer layer4 = new Layer(listShape4, 800, 900, 1, 6, true);
        Layer layer5 = new Layer(listShape5, 900, 100, 3, 5, true);

        /**
         * thêm các layer vào listLayer
         */
        ArrayList<Layer> listLayer = new ArrayList<Layer>();
        listLayer.add(layer1);
        listLayer.add(layer2);
        listLayer.add(layer3);
        listLayer.add(layer4);
        listLayer.add(layer5);

        /**
         * Khởi tạo diagram chứa listLayer
         */
        Diagram diagram = new Diagram(listLayer, 300, 700);
        //layer1.deleteLayer();
        //layer3.deleteLayer();
        //layer4.deleteLayer();
        //System.out.println(diagram.toString());
        //System.out.println(layer1.toString());
        /**
         * Nhóm các shape vào groups
         */
        diagram.groupShape();
        //System.out.println(layer4.toString());
        //System.out.println('\n');

        /**
         * In ra thông tin của diagram
         */
        System.out.println(diagram.toString());

        //System.out.println(shape1.toString());
        //System.out.println(shape1.toString());
        //System.out.println(shape1.toString());
        //System.out.println(shape3.toString());
        //System.out.println(shape4.toString());

        //System.out.println(listShape1.toString());
        //System.out.println(listShape2.toString());

        //System.out.println(layer1.toString());
        //System.out.println(layer2.toString());

        //System.out.println(diagram.toString());

        //diagram.removeAllCircle();
        //System.out.println(diagram.toString());

        //layer2.removeTriangle();
        //System.out.println(layer2.toString());

    }
}
