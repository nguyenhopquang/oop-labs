/**
 * Class Square ke thua tu class Square
 * @author Nguyen Hop Quang
 * @version 1.0
 */
public class Square extends Rectangle {
    public Square() {}
    public Square(int side) {
        super(side, side);
    }

    /**
     * Hàm khởi tạo của Class Square với 3 tham số đầu vào
     * @param side la canh
     * @param cl la color
     * @param f la filled
     */
    public Square(int side, String cl, boolean f, int x, int y) {
        super(side, side, cl, f, x, y);
    }

    /**
     * Getter/Setter
     * @return
     */
    public int getSide() {
        return super.getWidthR();
    }

    public void setSide(int side) {
        super.setWidth(side);
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(int length) {
        super.setLength(length);
    }

    /**
     * Hàm in ra thông tin
     * @return trả về màu sắc và isfilled và side
     */
    @Override
    public String toString() {
        return "Shape: Square\n"+ "Color: "+ getColor() + "\nSide: " + getSide() + "\nIsFilled: " + isFilled() + "\n";
    }
    /**
     * Hàm kiểm tra xem một đầu vào có trungf với Object không
     * @param obj là đối tượng đầu vào
     * @return kết quả true hoặc false
     */

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(getClass() != obj.getClass()) return false;
        else {
            Object other_square = (Square)obj;
            return (((Square) other_square).getSide() == getSide() &&((Square) other_square).getPos1() == getPos1() && ((Square) other_square).getPos2() == getPos2());
        }
    }
}