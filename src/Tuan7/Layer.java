/**
 * Class Layer ke thua tu class Diagram
 * @author Nguyen Hop Quang
 * @version 1.0
 */
import java.util.ArrayList;

public class Layer extends Diagram{
    protected double pos1, pos2;
    protected boolean visible = true;
    public ArrayList<Shape> listShape = new ArrayList<>();
    public ArrayList<Shape> newListShape = new ArrayList<Shape>();

    /**
     * Getter Setter
     */
    public double getPos1() {
        return pos1;
    }

    public void setPos1(double pos1) {

        this.pos1 = pos1;
    }

    public double getPos2() {

        return pos2;
    }

    public void setPos2(double pos2)
    {
        this.pos2 = pos2;
    }

    public Layer() {}

    public boolean isVisible() {

        return visible;
    }

    public void setVisible(boolean visible) {

        this.visible = visible;
    }

    /**
     * Tạo layer mới
     * @param pos1 là vị trí 1
     * @param pos2 là vị trí 2
     * @param visible là thuộc tính visible
     */
    public Layer(double pos1, double pos2, boolean visible) {
        this.pos1 = pos1;
        this.pos2 = pos2;
        this.visible = visible;
    }

    /**
     * Tạo layer mới
     * @param w là width
     * @param h là height
     * @param pos1 là vị trí 1
     * @param pos2 là vị trí 2
     * @param visible là visible
     */
    public Layer(double w, double h, double pos1, double pos2, boolean visible) {
        super(w, h);
        setPos1(pos1);
        setPos2(pos2);
        setVisible(visible);
    }

    /**
     * Thêm list vào layer
     * @param list là list cần thêm
     * @param w là width
     * @param h laf height
     * @param x la toa do x
     * @param y la toa do y
     * @param visible la visible
     */
    public Layer(ArrayList<Shape> list, double w, double h, double x, double y, boolean visible) {
        super(w, h);
        setPos2(y);
        setPos1(x);
        setVisible(visible);
        listShape.clear();
        listShape.addAll(list);
    }

    /**
     * Them list vafo layer
     * @param list
     */
    public Layer(ArrayList<Shape> list) {
        super();
        listShape.clear();
        listShape.addAll(list);
    }

    /**
     * Hàm xoá tất cả các đối tượng thuộc lớp Triangle
     */
    public void removeTriangle() {
        ArrayList<Shape> listWithoutTriangle = new ArrayList<Shape>();
        for(Shape s : listShape) {
            if(!(s instanceof Triangle)) {
                listWithoutTriangle.add(s);
            }
            listShape = listWithoutTriangle;
        }
    }

    /**
     * Hàm xóa tất cả layer trùng nhau
     */
    public void deleteLayer () {
        for(int i = 0; i < listShape.size(); i++) {
            for(int j = i + 1; j < listShape.size(); j++) {
                if(listShape.get(i).equals(listShape.get(j))) {
                    if(!newListShape.contains(listShape.get(i))) {
                        newListShape.add(listShape.get(i));
                    }
                }
            }
        }
        listShape = newListShape;
    }

    /**
     * Hàm trả về thông tin
     * @return thông tin của listShape
     */
    @Override
    public String toString() {
        if(isVisible()) {
            String a = "Layer \n" + "Position 1: "+getPos1() + "\nPosition 2: " + getPos2() +"\nVisible :"+ isVisible() +"\n";
            a += "Height: " + getHeight() + "\nWeight: " + getWidth() + "\n";
            for(int i = 0; i < listShape.size(); i++) {
                a += listShape.get(i).toString();
            }
            return a;
        }
        else
        {
            return "";
        }

    }
}
