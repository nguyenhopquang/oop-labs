/**
 * Class  Diagram
 * @author Nguyen Hop Quang
 * @version 1.0
 */
import java.util.ArrayList;

public class Diagram
{
    protected double width, height;

    protected ArrayList<Layer> listLayers = new ArrayList<Layer>();

    /**
     * Getter/Setter
     */
    public double getWidth ()
    {
        return width;
    }

    public double getHeight ()
    {
        return height;
    }

    public void setHeight (double height)
    {
        this.height = height;
    }

    public void setWidth (double width)
    {
        this.width = width;
    }

    /**
     * constructor rong
     */
    public Diagram(){};

    /**
     * constructor khoi tao chieu dai, rong
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(double w, double h)
    {
        setHeight(h);
        setWidth(w);
    }

    /**
     * constructor khoi tao nhieu layer
     * @param list danh sach layer
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(ArrayList<Layer> list, double w, double h)
    {
        setWidth(w);
        setHeight(h);
        listLayers.clear();
        listLayers.addAll(list);
    }


    /**
     * ham xoa cac doi tuong thuoc class Circle
     */
    public void removeAllCircle()
    {
        ArrayList<Shape> listWithoutCircle = new ArrayList<Shape>();
        for (Layer layer: listLayers)
        {
            for (Shape shape : layer.listShape)
                if (!(shape instanceof Circle)) listWithoutCircle.add(shape);
            layer.listShape = listWithoutCircle;
        }
    }

    /**
     * Hàm tạo group cho các loại Shape khác nhau, lưu vào arrayList
     */
    public void groupShape() {
        ArrayList<Shape> listSquare = new ArrayList<Shape>();
        ArrayList<Shape> listRectangle = new ArrayList<Shape>();
        ArrayList<Shape> listTriangle = new ArrayList<Shape>();
        ArrayList<Shape> listCircle = new ArrayList<Shape>();
        ArrayList<Shape> listHexagon = new ArrayList<Shape>();
        for(int i = 0; i < listLayers.size(); i++) {
            for(Shape shape : listLayers.get(i).listShape) {
                if(shape instanceof Square) listSquare.add(shape);
                else if(shape instanceof Rectangle) listRectangle.add(shape);
                else if(shape instanceof Triangle) listTriangle.add(shape);
                else if(shape instanceof Circle) listCircle.add(shape);
                else if(shape instanceof Hexagon) listHexagon.add(shape);
            }
        }

        Layer layer0 = new Layer(listSquare);
        Layer layer1 = new Layer(listRectangle);
        Layer layer2 = new Layer(listTriangle);
        Layer layer3 = new Layer(listCircle);
        Layer layer4 = new Layer(listHexagon);

        listLayers.clear();

        System.out.println("Kich co layer la : "+ listLayers.size());
        listLayers.add(layer0);
        listLayers.add(layer1);
        listLayers.add(layer2);
        listLayers.add(layer3);
        listLayers.add(layer4);

    }
    /**
     * Hàm trả về thông tin
     * @return thông tin của listDiagram
     */
    @Override
    public String toString() {
        String a = "Diagram: \n" +" " + "Weight: " + getWidth() + "; Height: " + getHeight() + "\n";
        for(int i = 0; i < listLayers.size(); i++) {
            a += listLayers.get(i).toString();
        }
        return a;
    }
}
