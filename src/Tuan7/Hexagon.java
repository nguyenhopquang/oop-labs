/**
 * Class Square ke thua tu class Square
 * @author Nguyen Hop Quang
 * @version 1.0
 */
public class Hexagon extends Shape {
    public int side1, side2, side3, side4, side5, side6;

    public Hexagon(int side1, int side2, int side3, int side4, int side5, int side6) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.side4 = side4;
        this.side5 = side5;
        this.side6 = side6;
    }
    public Hexagon(int side1, int side2, int side3, int side4, int side5, int side6, int x, int y, String cl, boolean f) {
        super(cl, f, x, y);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.side4 = side4;
        this.side5 = side5;
        this.side6 = side6;
    }

    /**
     * Constructor rỗng
     */
    public Hexagon() {
    }

    /**
     * Getter/Setter
     */
    public int getSide1() {
        return side1;
    }

    public void setSide1(int side1) {
        this.side1 = side1;
    }

    public int getSide2() {
        return side2;
    }

    public void setSide2(int side2) {
        this.side2 = side2;
    }

    public int getSide3() {
        return side3;
    }

    public void setSide3(int side3) {
        this.side3 = side3;
    }

    public int getSide4() {
        return side4;
    }

    public void setSide4(int side4) {
        this.side4 = side4;
    }

    public int getSide5() {
        return side5;
    }

    public void setSide5(int side5) {
        this.side5 = side5;
    }

    public int getSide6() {
        return side6;
    }

    public void setSide6(int side6) {
        this.side6 = side6;
    }
    /**
     * Hàm trả về thông tin
     * @return
     */
    @Override
    public String toString() {
        return "Shape: Hexagon\n" + "Side1:" + getSide1() + "\nSide2: " + getSide2() + "\nSide3: "  + getSide3()
                + "\nSide4:" + getSide4() + "\nSide5: " + getSide5() + "\nSide6: "  + getSide6() +"\nColor: " + getColor() + "" +
                "\nIsFilled: " + isFilled() +"\n";
    }

    /**
     * Hàm kiểm tra xem một đầu vào có trungf với Object không
     * @param obj là đối tượng đầu vào
     * @return kết quả true hoặc false
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        else if(getClass() != obj.getClass()) return false;
        else {
            Object other_hexagon = (Hexagon)obj;
            return (((Hexagon) other_hexagon).getSide1() == getSide1() && ((Hexagon) other_hexagon).getSide2() == getSide2() && ((Hexagon) other_hexagon).getSide3() == getSide3() && ((Hexagon) other_hexagon).getSide4() == getSide4() && ((Hexagon) other_hexagon).getSide5() == getSide5() && ((Hexagon) other_hexagon).getSide6() == getSide6() && ((Hexagon) other_hexagon).getPos1() == getPos1() && ((Hexagon) other_hexagon).getPos2() == getPos2());
        }
    }
}
