/**
 * Lớp triangle quản lí hình CN
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Rectangle extends Shape {
    private double w, h;

    /**
     * Hàm khởi tạo HCN
     */
    public Rectangle() {
        super("rectangle");
    }

    /**
     * Hàm khởi tạo HCN với thông số
     * @param Color là màu HCN
     * @param x là tọa độ hoành độ
     * @param y là tung độ
     */
    public Rectangle(String Color, double x, double y) {
        super("rectangle", Color, x, y);
    }

    /**
     *
     * Hàm khởi tạo HCN với thông số
     * @param Color là màu HCN
     * @param x là tọa độ hoành độ
     * @param y là tung độ
     * @param W là chiều dài
     * @param H là chiều rộng
     */
    public Rectangle(String Color, double x, double y, double W, double H) {
        super("rectangle", Color, x, y);
        w = W;
        h = H;
    }
    /**
     * Getter/Setter
     */
    public double getH() {

        return h;
    }
    public double getW() {

        return w;
    }

    public void setH(double h) {
        this.h = h;
    }

    public void setW(double w) {

        this.w = w;
    }

    /**
     * hàm show in ra màn hình loại hình, thông số của HCN
     */
    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getH() + "x" + getW());
    }
}
