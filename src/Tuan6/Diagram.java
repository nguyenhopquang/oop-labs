import java.util.*;
/**
 * Lớp diagram quản lí các layer
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Diagram {

    /**
     * Khởi tạo 1 list layer
     */
    private ArrayList<Layer> layerList = new ArrayList<>();

    /**
     * Hàm thực hiện xóa hình tròn trong sơ đồ
     */
    public void deleteCircle() {
        for(int i = 0; i<layerList.size(); i++) {
            layerList.get(i).deleteCircle();
        }
    }

    /**
     * Thêm layer vào trong mảng
     * @param layer
     */
    public void add(Layer layer) {

        layerList.add(layer);
    }

    /**
     *Hiển thị tất cả các hình trong layer
     */
    public void showAll() {
        for(int i = 0; i < layerList.size(); i++) {
            layerList.get(i).showAll();
        }
    }
}
