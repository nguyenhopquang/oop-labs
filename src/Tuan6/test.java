public class test {
    /**
     * Hàm main khởi tạo các hình, in các hình ra màn hình và gọi các hàm xóa
     * @param args không sử dụng
     */
    public static void main(String[] args) {
        Diagram diagram = new Diagram();
        Shape shape1 = new Rectangle("red", 1, 1, 3, 5);

        Shape shape2 = new Circle("red", 1, 1, 3);
        Shape shape3 = new Circle("blue", 1, 1, 3);

        Shape shape4 = new Triangle("blue",1,1,3,4,5);
        Shape shape5 = new Triangle("black",1,1,3,4,5);

        Layer layer1 = new Layer();

        layer1.add(shape1);
        layer1.add(shape2);
        layer1.add(shape3);
        layer1.add(shape4);
        layer1.add(shape5);

        diagram.add(layer1);
        diagram.showAll();

        System.out.println("Xoa hinh");
        System.out.println("--------------------");
        diagram.deleteCircle();
        layer1.deleteTriangle();


        diagram.showAll();
    }
}
