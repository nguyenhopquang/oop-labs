/**
 * Lớp triangle quản lí hình tam giác
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Triangle extends Shape {
    private double a, b, c;

    /**
     * Hàm khởi tạo hình tam giác
     */
    public Triangle() {

        super("triangle");
    }

    /**
     * Hàm khởi tạo hình tam giác theo thông số có sẵn
     * @param Color là màu của hình tam giác
     * @param x là tọa độ hoành độ
     * @param y là tọa độ tung độ
     */
    public Triangle(String Color, double x, double y) {
        super("triangle", Color, x, y);
    }

    /**
     * Hàm khởi tạo hình tam giác với thông số cho trước
     * @param Color là màu của hình tam giác
     * @param x là tọa độ hoành độ
     * @param y là tọa độ tung độ
     * @param A là độ dài cạnh tam giác
     * @param B là độ dài cạnh tam giác
     * @param C là độ dài cạnh tam giác
     */
    public Triangle(String Color, double x, double y, double A, double B, double C) {
        super("triangle", Color, x, y);
        a = A;
        b = B;
        c = C;
    }

    /**
     * Getter/ Setter
     */
    public double getA() {
        return a;
    }

    public double getB() {

        return b;
    }

    public double getC() {
        return c;
    }

    public void setA(double a) {

        this.a = a;
    }

    public void setB(double b) {

        this.b = b;
    }

    public void setC(double c) {

        this.c = c;
    }

    /**
     * hàm show in ra màn hình loại hình, thông số của hình tam giác
     */
    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getA() + "x" + getB() + "x" + getC());
    }
}
