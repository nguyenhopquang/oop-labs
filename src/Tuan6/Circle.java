/**
 * Lớp triangle quản lí hình tam giác
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */

public class Circle extends Shape {
    private double r;

    /**
     * Hàm khởi tạo hình tròn
     */
    public Circle() {
        super("circle");
    }

    /**
     * Hàm khởi tạo thông số cho hình tròn
     * @param Color là màu
     * @param x là tọa độ hoành độ
     * @param y là tọa độ tung độ
     */
    public Circle(String Color, double x, double y) {

        super("circle", Color, x, y);
    }

    /**
     * Hàm khởi tạo thông số cho hình tròn
     * @param Color là màu
     * @param x là hoành độ
     * @param y là tung độ
     * @param R là bán kính
     */
    public Circle(String Color, double x, double y, double R) {
        super("circle", Color, x, y);
        r = R;
    }

    /**
     * Getter/ Setter
     */
    public double getR() {

        return r;
    }

    public void setR(double r) {

        this.r = r;
    }

    /**
     * hàm show in ra màn hình loại hình, thông số của hình tròn
     */
    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getR());
    }
}
