/**
 * Lớp triangle quản lí hình vuông
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Square extends Shape {
    private double size;

    public Square() {

        super("square");
    }
    public Square(String Color, double x, double y) {

        super("square", Color, x, y);
    }
    public Square(String Color, double x, double y, double Size) {
        super("square", Color, x, y);
        size = Size;
    }

    /**
     * Getter/Setter
     * @return
     */
    public double getSize() {

        return size;
    }

    public void setSize(double size) {

        this.size = size;
    }

    /**
     * hàm show in ra màn hình loại hình, thông số của hình vuông
     */
    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getSize());
    }
}

