import java.util.*;

/**
 * Lớp layer quản lí danh sách các hình
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Layer {

    /**
     * Khởi tạo 1 list shape
     */
    private ArrayList<Shape> shapeList = new ArrayList<>();

    /**
     * Hàm thực hiện xóa các hình tam giác trong 1 layer
     */
    public void deleteTriangle() {
        for(int i=shapeList.size()-1; i>=0; i--) {
            if(shapeList.get(i).getType().equals("triangle")) shapeList.remove(i);
        }
    }

    /**
     * Hàm thực hiện xóa hình tròn trong 1 layer
     */
    public void deleteCircle() {
        for(int i = shapeList.size()-1; i>=0; i--) {
            if(shapeList.get(i).getType().equals("circle")) shapeList.remove(i);
        }
    }

    /**
     * Hàm hiển thị tất cả các hình trong layer
     */
    public void showAll() {
        for(int i = 0; i<shapeList.size(); i++) {
            shapeList.get(i).show();
        }
    }

    /**
     * Hàm thêm hình vào layer
     * @param shape
     */
    public void add(Shape shape) {
        shapeList.add(shape);
    }

}
