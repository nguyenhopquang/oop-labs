/**
 * Lớp shape quản lí các hình
 * @author Nguyễn Hợp Quang
 * @version 1.0
 */
public class Shape {
    private String type;
    private String color;
    private double x, y;

    /**
     * Khởi tạo hình có màu trắng
     * @param Type là loại hình được truyền vào
     */
    public Shape(String Type) {
        x = y = 0;
        type = Type;
        color = "white";
    }

    /**
     * Hàm khởi tạo hình với thông số cho trước
     * @param Type là loại hình
     * @param Color là màu
     * @param X là hoành độ
     * @param Y là tung độ
     */
    public Shape(String Type, String Color, double X, double Y) {
        type = Type;
        color = Color;
        x = X;
        y = Y;
    }

    /**
     * Getter/Setter
     */
    public void setColor(String Color) {

        color = Color;
    }

    public void setType(String Type) {

        type = Type;
    }

    public void setX(double X) {
        x = X;
    }

    public void setY(double Y) {

        y = Y;
    }

    public String getType() {

        return type;
    }

    public String getColor() {

        return color;
    }

    public double getX() {

        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Hàm di chuyển vị trí hình
     * @param changeX là độ dịch hoành độ
     * @param changeY là độ dịch tung độ
     */
    public void move(int changeX, int changeY) {
        x += changeX;
        y += changeY;
    }

    /**
     * hàm show in ra màn hình loại hình, thông số của hình tam giác
     */
    public void show() {
    }
}
