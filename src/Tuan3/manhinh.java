/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class manhinh {
    private String kichco, ten, hang;
    /**
     * Getter/Setter
     */
    public String getKichco() {
        return kichco;
    }

    public void setKichco(String kichco) {
        this.kichco = kichco;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getHang() {
        return hang;
    }

    public void setHang(String hang) {
        this.hang = hang;
    }
}