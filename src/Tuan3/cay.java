/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class cay {
    private String ten, cao, loai;
    /**
     * Getter/Setter
     */
    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getCao() {
        return cao;
    }

    public void setCao(String cao) {
        this.cao = cao;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }
}