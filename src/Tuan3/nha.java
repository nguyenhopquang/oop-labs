/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class nha {
    private String socua, namxaydung, loainha;
    /**
     * Getter/Setter
     */
    public String getSocua() {
        return socua;
    }

    public void setSocua(String socua) {
        this.socua = socua;
    }

    public String getNamxaydung() {
        return namxaydung;
    }

    public void setNamxaydung(String namxaydung) {
        this.namxaydung = namxaydung;
    }

    public String getLoainha() {
        return loainha;
    }

    public void setLoainha(String loainha) {
        this.loainha = loainha;
    }
}