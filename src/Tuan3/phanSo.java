/**
 * Cong tru nhan chia va so sanh phan so
 * @author nguyenhopquang
 * @version 1.0
 */
public class phanSo {
    //Khai bao thuoc tinh mac dinh tu va mau
    private  int tu;
    private int mau;

    /**
     *Getter / Setter
     */
    public int getTu() {

        return this.tu;
    }

    public void setTu(int tu) {
        this.tu = tu;
    }

    public int getMau()
    {
        return this.mau;
    }

    public void setMau(int mau) {

        this.mau = mau;
    }

    /**
     * Ham tim uoc chung lon nhat cua hai so
     * @param a la so thu nhat
     * @param b la so thu 2
     * @return tra ve ucln cua hai so
     */
    public int timUSCLN(int a, int b) {
        while (a != b) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    }

    /**
     * Ham tim BSCNN cua hai so a va b
     * @param a la so thu nhat
     * @param b la so thu 2
     * @return tra ve ket qua la BSCNN cua a va b
     */
    public int timBSCNN(int a, int b) {
        return a*b/timUSCLN(a,b);
    }
    /**
     * Ham khoi tao co tham so cho tu va mau
     * @param tu la tu so cua phan so
     * @param mau la mau so cua phan so
     */
    public phanSo(int tu, int mau) {
        this.tu= tu;
        this.mau= mau;
    }

    /**
     * Ham cong hai phan so nhan vao hai phan so ps1 va ps2
     * @param ps1 la phan so thu nhat
     * @param ps2 la phan so thu 2
     * Ham in ra man hinh ket qua cua phep cong hai phan so
     */
    public void congPhanSo(phanSo ps1, phanSo ps2 ) {
        int mc = timBSCNN(ps1.getMau(), ps2.getMau());
        phanSo result = new phanSo(1,2);

        result.setTu(ps1.getTu()*mc /ps1.getMau()+ps2.getTu()*mc/ps2.getMau());
        result.setMau(mc);
        System.out.println(result.getTu()+"/"+result.getMau());
    }

    /**
     * Ham tru hai phan so ps1 va ps2
     * @param ps1 la phan so 1
     * @param ps2 la phan so 2
     * Ham in ra man hinh ket qua phep tru hai phan so
     */
    public void truPhanSo(phanSo ps1, phanSo ps2) {
        int mc = timBSCNN(ps1.getMau(), ps2.getMau());
        phanSo result = new phanSo(1,2);
        result.setTu(ps1.getTu()*mc/ps1.getMau()-ps2.getTu()*mc/ps2.getMau());
        result.setMau(mc);
        System.out.println(result.getTu()+"/"+result.getMau());
    }

    /**
     * Ham nhan hai phan so ps1 va ps2
     * @param ps1 la phan so 1
     * @param ps2 la phan so 2
     * Ham in ket qua ra man hinh
     */
    public void nhanPhanSo(phanSo ps1, phanSo ps2) {
        int tu= ps1.getTu()*ps2.getTu();
        int mau= ps1.getMau()*ps2.getMau();
        System.out.println(tu + "/"+mau);
    }

    /**
     * Ham chia hai phan so ps1 va ps2
     * @param ps1 la phan so thu nhat
     * @param ps2 la phan so thu 2
     * Ham in ket qua ra man hinh
     */
    public void chiaPhanSo(phanSo ps1, phanSo ps2) {
        int tu= ps1.getTu()* ps2.getMau();
        int mau= ps1.getMau()* ps2.getTu();
        System.out.println(tu+"/"+mau);
    }
    /**
     * Ham main thuc hien in ra man hinh ket qua
     * @param args la tham so dau vao
     * Ham in ket qua ra man hinh
     */
    public boolean equals(Object obj) {
        phanSo ps= (phanSo) obj;
        return (ps.getTu()*this.mau == ps.getMau()*this.tu);
    }
    public static void main(String[] args) {
        phanSo ps1= new phanSo(2,3);
        phanSo ps2= new phanSo(3,4);
        phanSo test = new phanSo(4,5);
        //In ra cong phan so
        test.congPhanSo(ps1, ps2);
        //In ra chia phan so
        test.chiaPhanSo(ps1, ps2);
        //In ra nhan phan so
        test.nhanPhanSo(ps1, ps2);
        //In ra chia phan so
        test.truPhanSo(ps1,ps2);
        //In ra xem hai phan so co bang nhau khong
        System.out.println(ps1.equals(ps2));
    }

}