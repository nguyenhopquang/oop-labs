/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class ban {
    private int dai, rong, cao;
    /**
     * Getter/Setter
     */
    public int getDai() {
        return dai;
    }

    public void setDai(int dai) {
        this.dai = dai;
    }

    public int getRong() {
        return rong;
    }

    public void setRong(int rong) {
        this.rong = rong;
    }

    public int getCao() {
        return cao;
    }

    public void setCao(int cao) {
        this.cao = cao;
    }
}