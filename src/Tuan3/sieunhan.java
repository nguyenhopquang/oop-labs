/**
 * Muoi doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class sieunhan {
    //Khai bao thuoc tinh mac dinh
    private int chieucao;
    private int cannang;
    private int gioitinh;
    /**
     * Getter/Setter
     */
    public int getChieucao() {
        return chieucao;
    }

    public void setChieucao(int chieucao) {
        this.chieucao = chieucao;
    }

    public int getCannang() {
        return cannang;
    }

    public void setCannang(int cannang) {
        this.cannang = cannang;
    }

    public int getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(int gioitinh) {
        this.gioitinh = gioitinh;
    }

}