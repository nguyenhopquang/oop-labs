/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class hoa {
    private String ten, loai, mui;
    /**
     * Getter/Setter
     */
    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public String getMui() {
        return mui;
    }

    public void setMui(String mui) {
        this.mui = mui;
    }
}