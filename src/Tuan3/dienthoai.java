/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class dienthoai {
    private String hang, ten;
    private int namsanxuat;
    /**
     * Getter/Setter
     */
    public String getHang() {
        return hang;
    }

    public void setHang(String hang) {
        this.hang = hang;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getNamsanxuat() {
        return namsanxuat;
    }

    public void setNamsanxuat(int namsanxuat) {
        this.namsanxuat = namsanxuat;
    }
}