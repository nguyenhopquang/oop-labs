/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class maytinh {
    private String loai, ten, mausac;
    /**
     * Getter/Setter
     */
    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMausac() {
        return mausac;
    }

    public void setMausac(String mausac) {
        this.mausac = mausac;
    }
}