/**
 * 10 doi tuong
 * @author nguyenhopquang
 * @version 1.0
 */
public class oto {
    private String hangxe, namsx, loaixe;
    /**
     * Getter/Setter
     */
    public String getHangxe() {
        return hangxe;
    }

    public void setHangxe(String hangxe) {
        this.hangxe = hangxe;
    }

    public String getNamsx() {
        return namsx;
    }

    public void setNamsx(String namsx) {
        this.namsx = namsx;
    }

    public String getLoaixe() {
        return loaixe;
    }

    public void setLoaixe(String loaixe) {
        this.loaixe = loaixe;
    }
}