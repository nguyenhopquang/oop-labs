/**
 * Tim UCLN va Fibonaxi
 * @author nguyenhopquang
 * @version 1.0
 */
public class giaiToan {
    /**
     * Ham tim UCLN cua hai so
     * @param a la so dau tien
     * @param b la so thu 2
     * @return tra ve ket qua la UCLN
     */
    public int timUSCLN(int a, int b) {
        int ucln =0;
        if (a<0) a=-a;
        if (b<0) b= -b;
        while (a!=b) {
            if (a>b) a -=b; else b-=a;
        }
         ucln =a;
        return a;
    }

    /**
     * Ham Tinh fibonaxi cua mot so nguyen
     * @param n la so nguyen nhap vao
     * @return tra ve gia tri cua fibonaxi n
     */
    public int Fibonaxi (int n) {
        int a,b,c;
        if (n<0 || n==0) return 0;
        if (n==1) return 1;
        else {
            a=0;
            b=1;
            c=a+b;
            for (int i=3;i<n;i++) {
                a=b;
                b=c;
                c=a+b;
            }
        }
        return c;
    }

    /**
     * Ham main thuc hien in ra man hinh ket qua cua ucln va fibonaci
     * @param args la cac tham so dau vao
     * Ham in ket qua ra man hinh
     */
    public static void main (String[] args) {
        //Khoi tao bien toan bang new giaiToan
        giaiToan toan = new giaiToan();
        //In ra man hinh ket qua
        System.out.println(toan.timUSCLN(3,4));
        System.out.println(toan.Fibonaxi(6));
    }
}