public class max {
    /**
     * Hàm tìm số lớn nhất trong hai số nguyên
     * @param a là số nguyên thứ nhất
     * @param b là số nguyên thứ hai
     * @return trả về giá trị là số lớn nhất
     */
    public static int timMax(int a,int b) {
        if (a>b) {
            return a;
        } else {
            return b;
        }
    }
}
