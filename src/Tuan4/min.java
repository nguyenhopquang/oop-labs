public class min {
    /**
     * Hàm tìm số nhỏ nhất trong một mảng
     * @param mang là mảng chứa số nguyên
     * @param soluong là số phần tử của mảng
     * @return trả về số lớn nhất trong mảng
     */
    public static int timMin(int[] mang, int soluong) {
        int min=mang[0];
        for(int i=0;i<soluong;i++) {
            if(mang[i]<min) {
                min=mang[i];
            }
        }
        return min;
    }
}
