import org.junit.Test;

import static org.junit.Assert.*;

public class bmiTest {

    @Test
    public void test1() {
        bmi test = new bmi();
        String result1 = test.tinhBMI(74,1.74);

        assertEquals("Thừa cân", result1);
    }
    @Test
    public void test2() {
        bmi test = new bmi();
        String result2 = test.tinhBMI(45,1.64);
        assertEquals("Thiếu cân", result2);
    }
    @Test
    public void test3() {
        bmi test = new bmi();
        String result3 = test.tinhBMI(67,1.72);
        assertEquals("Bình thường", result3);
    }
    @Test
    public void test4() {
        bmi test = new bmi();
        String result4 = test.tinhBMI(78,1.70);
        assertEquals("Béo phì", result4);
    }
    @Test
    public void test5() {
        bmi test = new bmi();
        String result5 = test.tinhBMI(85,1.80);
        assertEquals("Béo phì", result5);
    }
}