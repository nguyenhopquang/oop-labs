import java.io.*;
import java.util.Scanner;

/**
 *Chương trình dùng để tính và đưa ra kết luận về chỉ số BMI
 * @author nguyenhopquang
 * @version 13.03.1999
 */
public class bmi {
    public static String tinhBMI(double cannang, double chieucao) {
        double bmi= cannang/(chieucao*chieucao);
        if (bmi<18.5) {
            return "Thiếu cân";
        } else if (bmi<22.99) {
            return "Bình thường";
        } else if (bmi<24.99) {
            return "Thừa cân";
        } else return "Béo phì";
    }
    /**
     * Hàm main nhận các biến chiều cao và cân nặng
     * @param args không dùng
     * Hàm in ra kết luận từ chỉ số BMI
     */
    public static void main(String[] args) {

    }
}
