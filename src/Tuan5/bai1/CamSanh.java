
class CamSanh extends Cam{

    /**
     * Định nghĩa lại get color đã có
     * @return trả về màu của cam
     */
    public String getColor() {
        return "Xanh";
    }

    /**
     * Constructor không có tham số
     */
    public CamSanh() {
        this.soluong=1;
        this.price=13000;
    }

    /**
     * Constructor có tham số
     * @param soluong là số lượng quả
     * @param price là giá của cam
     */
    public CamSanh(int soluong, int price) {
        this.soluong=soluong;
        this.price=price;
    }

    /**
     * Ham tra ve gia tri la ten loai + nguon goc+ so luong o dang string
     * @return tra ve chuoi ket qua
     */
    public String toString() {

        return name+" "+ nguongoc+ " " +soluong+ " " +price+" "+this.getColor();
    }
}