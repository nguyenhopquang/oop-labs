class Cam extends Fruit{
    protected int price;
    protected String mausac;

    /**
     * Constructor không có tham số
     */
    public Cam() {
        this.nguongoc="Việt Nam";
        this.soluong=1;
        this.price=12000;
        this.mausac="Màu da cam";
        this.soluong=1;
    }

    /**
     * Constructor có tham số
     * @param nguongoc là nguồn gốc của quả cam
     * @param soluong là số lượng cam
     * @param price là giá của quả cam
     */
    public Cam(String nguongoc, int soluong, int price) {
        this.nguongoc=nguongoc;
        this.soluong=soluong;
        this.price=price;
    }

    /**
     * Định nghĩa lại phương thức get name
     * @return trả về tên mới
     */
    public String getName() {
        return "Quả cam";
    }

    /**
     * Getter and Setter
     * @param c
     */
    public void setColor(String c) {
        this.mausac=c;
    }
    public void setPrice(int p) {
        this.price=p;
    }
    public int getPrice(int p) {
        return this.price;
    }
    public String getColor() {
        return this.mausac;
    }

    /**
     * Ham tra ve gia tri la ten loai + nguon goc+ so luong o dang string
     * @return tra ve chuoi ket qua
     */
    public String toString() {

        return this.getName() + " " + nguongoc + " " + soluong + " " + price + " " + mausac;
    }
}