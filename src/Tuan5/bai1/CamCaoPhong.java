class CamCaoPhong extends Cam{

    /**
     * Định nghĩa lại hàm get nguồn gốc
     * @return
     */
    public String getNguongoc() {
        return "Cam Cao Phong";
    }

    /**
     * Constructor không có tham số
     */
    public CamCaoPhong() {
        this.soluong=1;
        this.price=1;
        this.mausac="Màu cam";
    }

    /**
     * Constructor có tham số
     * @param soluong là số lượng quả
     * @param price là giá của cam
     */
    public CamCaoPhong(int soluong, int price) {
        this.soluong=soluong;
        this.price=price;
    }

    /**
     * Ham tra ve gia tri la ten loai + nguon goc+ so luong o dang string
     * @return tra ve chuoi ket qua
     */
    public String toString() {

        return name+this.getNguongoc() + " " + soluong + " " + price + " " + mausac;
    }
}