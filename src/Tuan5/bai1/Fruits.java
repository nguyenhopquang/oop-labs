/**
 * class Fruit la class cha cua hoa qua
 * @author Nguyen Hop Quang
 * @version 13.03.1999
 */

public class Fruits {

	/**
	 * Hàm main in ra số lượng và nguồn gốc các quả
	 * @param args không sử dụng
	 */
	public static void main(String[] args) {
		Fruit fr=new Fruit();
	System.out.println(fr);

		Fruit fr2= new Fruit("Sầu riêng",15,"Miền Nam");
	System.out.println(fr2);

		Tao ap=new Tao();
	System.out.println(ap);

		Tao ap2=new Tao("Trung quốc",50,20000);
	System.out.println(ap2);

	Cam or=new Cam();
	System.out.println(or);

		Cam or2 =new Cam("Châu Âu",100,15000);
	System.out.println(or2);

	CamCaoPhong cp=new CamCaoPhong();
	System.out.println(cp);

		CamCaoPhong cp2=new CamCaoPhong(150,25000);
	System.out.println(cp2);

	CamSanh so=new CamSanh();
	System.out.println(so);

		CamSanh so2=new CamSanh(180,30000);
	System.out.println(so2);
	}
}
