class Tao extends Fruit{
    protected int price;
    protected String mausac;

    /**
     * Phuong thuc getName định nghĩa lại giá trị name từ phương thức Fruit
     * @return trả về tên mới cho lớp
     */
    public String getName() {
        return "Quả Táo";
    }

    /**
     * Constructor không có tham số
     */
    public Tao() {
        this.nguongoc="Việt Nam";
        this.soluong=1;
        this.price=10000;
        this.mausac="Màu vàng";
    }

    /**
     * Constructor có tham số
     * @param nguongoc là nguồn gốc của quả Táo
     * @param soluong là số lượng Táo
     * @param price là giá của quả Táo
     */
    public Tao(String nguongoc, int soluong, int price) {
        this.nguongoc=nguongoc;
        this.soluong=soluong;
        this.price=price;
    }

    /**
     * Getter and Setter
     */
    public void setColor(String c) {
        this.mausac=c;
    }
    public void setPrice(int p) {
        this.price=p;
    }
    public int getPrice(int p) {
        return this.price;
    }
    public String getColor() {
        return this.mausac;
    }

    /**
     * Ham tra ve gia tri la ten loai + nguon goc+ so luong o dang string
     * @return tra ve chuoi ket qua
     */
    public String toString() {

        return this.getName() + " " + nguongoc + " " + soluong + " " + price + " "+ mausac;
    }
}