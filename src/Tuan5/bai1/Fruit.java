class Fruit{
    protected String name;
    protected String nguongoc;
    protected int soluong;

    /**
     * Constructor Fruit khoi tao gia tri bang gia tri duoc truyen vao
     * @param ten la ten cua loai qua
     * @param nguongoc la nguon goc cua qua
     * @param soluong la so luong cua qua
     */
    public Fruit(String ten, int soluong ,String nguongoc) {
        this.name = ten;
        this.soluong = soluong;
        this.nguongoc = nguongoc;
    }

    /**
     * Constructor khong tham so khoi tao gia tri mac dinh neu duoc goi
     */
    public Fruit() {
        this.name="Cam";
        this.nguongoc="VietNam";
        this.soluong=1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNguongoc() {
        return nguongoc;
    }

    public void setNguongoc(String nguongoc) {
        this.nguongoc = nguongoc;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    /**
     * Ham tra ve gia tri la ten loai + nguon goc+ so luong o dang string
     * @return tra ve chuoi ket qua
     */
    public String toString() {

        return name + " " + nguongoc + " " + soluong;
    }
}