class Square extends Rectangle{

    /**
     * Constuctor không có tham số
     */
    public Square() {}
    /**
     * Constructor có tham số
     * @param side độ dài cạnh
     */
    public Square(double side) {
        super.setWidth(side);
        super.setLength(side);
    }
    /**
     * Constructor có tham số
     * @param side độ dài cạnh
     * @param color màu hình vuông
     * @param filled đổ màu hay không
     */
    public Square(double side, String color, boolean filled) {
        super.setWidth(side);
        super.setLength(side);
        super.setColor(color);
        super.setFilled(filled);
    }

    /**
     * Getter & Setter
     */

    public void setSide(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public void setWidth(double side) {
        super.setWidth(side);
        super.setLength(side);
    }
    public void D(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    public double getSide() {

        return this.getLength();
    }

    /**
     * Hàm biểu diễn thuộc tính dạng chuỗi
     * @return trả về chuỗi
     */
    public String toString() {

        return this.getWidth()+" "+this.getColor()+" "+this.isFilled();
    }
}