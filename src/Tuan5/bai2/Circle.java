class Circle extends Shape{
    final private double PI=Math.PI;
    private double radius =1.0;

    /**
     * Constuctor không có tham số
     */
    public Circle() {
        this.radius=1.0;
    }

    /**
     * Constructor có tham số
     * @param radius là bán kính hình tròn
     * @param color màu hình tròn
     * @param filled đổ màu hay ko
     */
    public Circle(double radius, String color, boolean filled) {
        this.radius=radius;
        this.setColor(color);
        this.setFilled(filled);
    }

    /**
     * Getter & Setter
     */
    public void setRadius(double r) {
        this.radius=r;
    }
    public double getRadius() {
        return this.radius;
    }

    /**
     * Tính diện tích hình tròn
     * @return diện tích hình tròn
     */
    public double getArea() {
        double area=Math.pow(PI,2)*this.radius;
        return area;
    }

    /**
     *Tính chu vi hình vuông
     * @return chu vi hình vuông
     */
    public double getPerimeter() {
        double perimeter=2*PI*this.radius;
        return perimeter;
    }

    /**
     * Hàm biểu diễn thuộc tính dạng chuỗi
     * @return trả về chuỗi
     */
    public String toString() {
        return this.getRadius()+" "+this.getColor()+" "+this.isFilled()+" "+this.getArea()+" "+this.getPerimeter();
    }
}
