class Shape {
    private String color="red";
    private boolean filled=true;

    /**
     * Constuctor không có tham số
     */
    public Shape() {
        this.color="white";
        this.filled=false;
    }

    /**
     * Constructor có tham số
     * @param color là màu của hình
     * @param filled thể hiện khả năng đổ màu
     */
    public Shape(String color,boolean filled) {
        this.color=color;
        this.filled=filled;
    }


    /**
     * Getter and setter
     */
    public void setColor(String c) {

        this.color=c;
    }
    public void setFilled(boolean f) {

        this.filled=f;
    }
    public String getColor() {

        return this.color;
    }
    public boolean isFilled() {
        if(this.filled==true)return true;
        return false;
    }

    /**
     * Hàm biểu diễn thuộc tính dạng chuỗi
     * @return trả về chuỗi
     */
    public String toString() {
        return color + " "+ filled;
    }
}