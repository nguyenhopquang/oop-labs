class Rectangle extends Shape{
    private double width=1.0;
    private double length=1.0;

    /**
     * Constuctor không có tham số
     */
    public Rectangle() {
        this.width=1.0;
        this.length=1.0;
    }
    /**
     * Constructor có tham số
     * @param width chiều rộng
     * @param length chiều dài
     */
    public Rectangle(double width, double length) {
        this.width=width;
        this.length=length;
    }
    /**
     *Constructor có tham số
     * @param width chiều rộng
     * @param length chiều dài
     * @param color màu của hình
     * @param filled đổ màu hay không
     */
    public Rectangle(double width, double length, String color, boolean filled) {
        this.setWidth(width);
        this.setLength(length);
        this.setColor(color);
        this.setFilled(filled);
    }

    /**
     * Getter & Setter
     */
    public void setWidth(double w) {

        this.width=w;
    }
    public void setLength(double l) {
        this.length=l;
    }
    public double getWidth() {

        return this.width;
    }
    public double getLength() {

        return this.length;
    }

    /**
     * Ham getArea tính diện tích HCN
     * @return double là diện tích HCN
     */
    public double getArea() {

        return this.width*this.length;
    }

    /**
     * Ham getPerimeter tính chu vi
     * @return double là chu vi
     */
    public double getPerimeter() {
        return 2*(this.width+this.length);
    }
    /**
     * Hàm biểu diễn thuộc tính dạng chuỗi
     * @return trả về chuỗi
     */
    public String toString() {

        return this.getWidth() +" "+this.getLength()+" "+this.getColor()+" "+this.isFilled()+" "+ this.getArea()+" "+this.getPerimeter();
    }
}