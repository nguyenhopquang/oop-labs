/**
 * Class shape quản lí kích thước, màu của shape
 * @author Nguyễn Hợp Quang
 * @version 13.03.99
 */

public class Shapes {

	/**
	 * Ham main in ra chu vi, kích thước các hình
	 * @param args không sử dụng
	 */
	public static void main(String[] args) {

		Shape sh=new Shape();
			System.out.println(sh);

		Shape sh1=new Shape("Xanh",true);
			System.out.println(sh1);

		Circle cr=new Circle();
			System.out.println(cr);

		Circle cr2=new Circle(4,"Vàng",false);
			System.out.println(cr2);

		Rectangle re=new Rectangle();
			System.out.println(re);

		Rectangle re2=new Rectangle(4,6);
			System.out.println(re2);

		Rectangle re3=new Rectangle(4,6,"Đen",true);
			System.out.println(re3);

		Square sq=new Square();
			System.out.println(sq);

		Square sq2=new Square(8);
			System.out.println(sq2);

		Square sq3=new Square(8,"Đỏ",false);
			System.out.println(sq3);
	}
}
