package Tuan8.B2;
/**
 * class IOExceptions
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */
import java.io.IOException;

public class IOExceptions
{
    /**
     * Ham check IOException
     * @throws IOException
     */
    public static void check() throws IOException
    {
        throw new IOException();
    }
    
    public static void main(String[] args) throws IOException
    {
        check();
    }
}
