package Tuan8.B2;
/**
 * class Arithmatic kiem tra loi chia cho 0
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */

public class Arithmetic {
    /**
     * ham chia 2 so
     * @param a so thu 1
     * @param b so thu 2
     * @throws Exception
     */
    public static void division(int a, int b) throws ArithmeticException
    {
        if (b==0) throw new ArithmeticException("Loi");
        System.out.println(a/b);
    }
    public static void main(String[] args)throws ArithmeticException {
        division(10,2);
        division(3,0);
    }
}
