package Tuan8.B2;
/**
 * class FileNotFound kiem tra su ton tai cua file
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileNotFound
{
    /**
     * ham doc file
     * @param path duong dan toi file
     * @throws Exception
     */
    public static void readFile(String path) throws FileNotFoundException
    {
        File file = new File(path);
        if (!file.exists()) throw new FileNotFoundException("Khong co");
        Scanner scan = new Scanner(file);
        int n = scan.nextInt();
        System.out.println(n);
    }
    
    public static void main(String[] args) throws FileNotFoundException
    {
        readFile("hihi.txt");
    }
}
