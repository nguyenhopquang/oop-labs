package Tuan8.B2;
/**
 * class intToBoolean kiem tra loi ep kieu
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */
public class ClassCast {
    /**
     * ham chuyen int thanh bool
     * @throws Exception
     */
    public static boolean intToBoolean(int n) throws ClassCastException
    {
        if (n<0 || n>1) throw new ClassCastException("khong hop le");
        return (n==1);
    }

    public static void main(String[] args)throws ClassCastException {
        intToBoolean(1);
        intToBoolean(8);
    }
}
