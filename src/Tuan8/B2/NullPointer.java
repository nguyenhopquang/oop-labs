package Tuan8.B2;
/**
 * class NullPointer kiem tra con tro null
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */
public class NullPointer {
    /**
     * ham in string
     * @param s string
     * @throws Exception
     */
    public static void printString(String s) throws NullPointerException
    {
        if (s == null) throw new NullPointerException("Rong");
        System.out.println(s);
    }
    public static void main(String[] args)throws NullPointerException {
        printString("b");
        printString(null);
    }
}
