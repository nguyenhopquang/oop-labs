package Tuan8.B1;
/**
 * class tru 2 bieu thuc
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */

public class Subtraction extends BinaryExpression {
    private Expression btleft;
    private Expression btright;
    /**
     * constructor
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Subtraction(Expression l, Expression r) {
        btleft = l;
        btright = r;
    }
    @Override
    public Expression Left() { return btleft; }
    @Override
    public Expression Right() { return btright; }
    
    public void setLeft(Expression btleft) {
        this.btleft = btleft;
    }
    
    public void setRight(Expression btright) {
        this.btright = btright;
    }
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s - %s", btleft, btright);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return btleft.evaluate() - btright.evaluate();
    }
}
