package Tuan8.B1;
/**
 * class Numeral tao so nguyen
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */

public class Numeral extends Expression {
    private int val;

    public int getValue() {
        return val;
    }

    public void setValue(int value) {
        this.val = value;
    }

    /**
     * constructor
     */
    public Numeral(){};

    /**
     * constructor
     * @param val gia tri
     */
    public Numeral(int val)
    {
        val = val;
    }
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {
        return String.valueOf(val);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return val;
    }
}
