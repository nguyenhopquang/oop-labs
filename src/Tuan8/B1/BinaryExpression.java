package Tuan8.B1;
/**
 *BinaryExpression cac phep toan nhi phan
 * @author nguyenhopquang
 */

public abstract class BinaryExpression extends Expression {
   
    public abstract Expression Left();
    public abstract Expression Right();
    
    /**
     * Ham in
     * @return String
     */
    @Override
    public abstract String toString();
    /**
     * Ham tinh gia tri
     * @return int
     */
    @Override
    public abstract int evaluate();
}
