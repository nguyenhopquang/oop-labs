package Tuan8.B1;
/**
 * class ExpressionTest kiem tra cac class
 * @author Nguyen Hop Quang
 * @version 1.0
 */

public class ExpressionTest {

    public static void main(String args[]) {

        Expression numeral1 = new Numeral(4);
        Expression square1 = new Square(numeral1);
        Expression numeral2 = new Numeral(6);
        Expression addition1 = new Addition(square1, numeral2);
        Expression square2 = new Square(addition1);

        System.out.print(square2.toString() + " = ");
        System.out.println(square2.evaluate());
        try {
            Expression division = new Division(square1,new Numeral(0));
            System.out.print(division.toString() + " = ");
            System.out.println(division.evaluate());
        }
        catch (Exception e)
        {
            if (e instanceof ArithmeticException) System.out.println("Chia cho 0\n");
        }





    }
}
