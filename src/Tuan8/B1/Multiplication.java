package Tuan8.B1;
/**
 * class Multiplication nhan 2 bieu thuc
 * @author Nguyen Hop Quang
 * @version 1.0
 */

public class Multiplication extends BinaryExpression {
    private Expression btleft;
    private Expression right;
    /**
     * constructor
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Multiplication(Expression l, Expression r) {
        btleft = l;
        right = r;
    }
    @Override
    public Expression Left() { return btleft; }
    @Override
    public Expression Right() { return right; }
    
    public void setLeft(Expression btleft) {
        this.btleft = btleft;
    }
    
    public void setRight(Expression right) {
        this.right = right;
    }

    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {
        return String.format("(%s) * (%s)", btleft, right);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return btleft.evaluate() * right.evaluate();
    }
}
