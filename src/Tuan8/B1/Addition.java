package Tuan8.B1;
/**
 * class Addition cong 2 bieu thuc
 * @author nguyenhopquang
 */


public class Addition extends BinaryExpression {
    private Expression left;
    private Expression right;
    /**
     * constructor
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Addition(Expression l, Expression r) {
        left = l;
        right = r;
    }
    @Override
    public Expression Left() { return left; }
    @Override
    public Expression Right() { return right; }
    /**
     * Ham in
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s + %s", left, right);
    }

    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return left.evaluate() + right.evaluate();
    }
}
