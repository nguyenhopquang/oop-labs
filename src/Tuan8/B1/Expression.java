package Tuan8.B1;
/**
 * abstract class Expression la bieu thuc
 * @author Nguyen Hop Quang
 * @version 1.0
 *
 */

public abstract class Expression {
    /**
     * Ham in bieu thuc
     * @return String
     */
    public abstract String toString();
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    public abstract int evaluate();
}
