/**
 * Class findmax dung de findmax dung template
 * @author nguyenhopquang
 */
import java.util.ArrayList;

public class FindMax
{
    public static <T extends Comparable> T findmax(ArrayList<T> arr)
    {
        if(arr==null || arr.size()==0) return null;
        T result = arr.get(0);
        for (T element : arr)
        {
            if(result.compareTo(element)<0) result = element;
        }
        return result;
    }

    /**
     *main findmax
     * @param args không dùng tới
     */
    public static void main(String[] args)
    {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(0); arr.add(9); arr.add(2); arr.add(7); arr.add(10);
        System.out.println("Lon nhat: ");
        System.out.println(findmax(arr));
    }
}
