package Tuan11;

/**
 * Class bubblesort de sort dung template
 * @author nguyenhopquang
 */
public class BubbleSort
{
    public static <T extends Comparable> void bubblesort(T[] arr)
    {
        for(int i = 0; i<arr.length; i++)
            for(int j =i+1; j <arr.length;j++)
            if(arr[i].compareTo(arr[j])>0)
            {
                T tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
    }
    /**
     * in ra ket qua
     * @param arr là
     * @param <T>
     */
    public static <T> void print(T[] arr)
    {
        for(T element : arr)
            System.out.print(element + "-");
        System.out.println();
    }
    
    public static void main(String[] args)
    {
        Integer[] arr = {8,2,5,1,5,9,2};
        System.out.println("Array: ");
        print(arr);
        bubblesort(arr);
        System.out.println("Sort: ");
        print(arr);
    }
}
