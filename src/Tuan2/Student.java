package Tuan2;
/**
 * Class quan ly sinh vien
 * @author Nguyen Hop Quang
 */

public class Student
{
    private String name, id, group, email;
    public String getName()
    {
        return name;
    }

    public String getId()
    {
        return id;
    }

    public String getEmail()
    {
        return email;
    }

    public String getGroup()
    {
        return group;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * Ham getInfo in ra thong tin day du cua sinh vien
     */
    public void getInfo()
    {
        System.out.printf("Ho va ten:", this.name);
        System.out.printf("Nhom:", this.group);
        System.out.printf("Email:", this.email);
        System.out.printf("MS: ", this.id);
    }
    /**
     * Constructor mac dinh
     */
    public Student()
    {
        setName("Sinh vien");
        setId("0000");
        setGroup("K59CB");
        setEmail("uet@vnu.edu.vn");
    }
    /**
     * Constructor co tham so
     * @param ten la ten sinh vien
     * @param id la mssv cua sinh vien
     * @param email la email cua sinh vien
     */
    public Student(String ten, String id, String email)
    {
        setName(ten);
        setId(id);
        setGroup("K59CB");
        setEmail(email);
    }

    /**
     * ham sao chep sinh vien
     * @param s la sinh vien can sao chep
     */
    public Student(Student s)
    {
        setName(s.getName());
        setId(s.getId());
        setGroup(s.getGroup());
        setEmail(s.getEmail());
    }
}
