package Tuan2;
/**
 * class quan ly sinh vien
 * @author Nguyen Hop Quang
 */

public class StudentManagement
{
    private Student[] students = new Student[50];
    public  int count = 0;

    /**
     * So sanh nhom 2 sinh vien
     * @param sv1 la sinh vien 1
     * @param sv2 la sinh vien 2
     */
    public static boolean checkGroup(Student sv1, Student sv2)
    {

        return sv1.getGroup() == sv2.getGroup();
    }
    /**
     * in ra sinh vien theo nhom
     */
    public void printByGroup()
    {
        boolean[] check = new boolean[count];
        for(int i = 0; i < count; i++) check[i] = true;

        for(int i = 0; i < this.count; i++)
            if (check[i] == true)
            {
                System.out.println(this.students[i].getGroup());
                for (int j = i; j < this.count; j++)
                    if (checkGroup(this.students[i],this.students[j]))
                    {
                        check[j] = false;
                        this.students[j].getInfo();
                    }
            }
    }
    /**
     * ham xoa sinh vien
     * @param id la mssv cua sinh vien do
     */
    public  void removeStudent(String id)
    {
        int i=0;
        for (i = 0; i < this.count-1; i++)
            if (this.students[i].getId()==id) break;
        for(int j = i; i <this.count; j++) this.students[i] = this.students[i+1];
        count--;
    }
    /**
     * ham them sinh vien
     * @param s la sinh vien do
     */
    public  void addStudent(Student s)
    {
        this.students[this.count] = new Student();
        this.students[this.count] = s;
        count++;

    }

    public static void main(String[] args)
    {
        Student a = new Student("Nguyen Hop Quang", "17021323", "quang@quang.com");
        StudentManagement p = new StudentManagement();
        a.setGroup("K59CLC");
        a.setName("ABC");
        a.setGroup("K59CB");
        p.printByGroup();

    }

}