package Tuan9;
/**
 * class Utils gom mot so thao tac voi file
 * @author Nguyen Hop Quang
 * @version 1.0
 */

import java.io.*;
import java.util.Scanner;

public class Utils
{
    /**
     * Doc file
     * @param duongdan duong dan toi file
     * @return noi dung file
     * @throws IOException
     */
    public static String readFromFile(String duongdan) throws IOException
    {
        String s = "";
        File file = new File(duongdan);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) s += scanner.nextLine()+"\n";
        return s;
    }
    
    /**
     * Tao file moi va viet vao file
     * @param duongdan duong dan toi file
     * @throws IOException
     */
    public static void vietFile(String duongdan) throws IOException
    {
        FileWriter fileWriter = new FileWriter(duongdan);
        fileWriter.write("mot hai ba");
        fileWriter.close();
    }
    
    /**
     * Ham viet bo sung them vao file
     * @param duongdan duong dan toi file
     * @throws IOException
     */
    public static void vietFile2(String duongdan) throws IOException
    {
        FileWriter fileWriter = new FileWriter(duongdan,true);
        fileWriter.write("4 5 6");
        fileWriter.close();
    }

    /**
     * Ham tim file
     * @param folderPath thu muc can tim
     * @param fileName ten file can tim
     * @return file
     */
    public static File findFileByName(String folderPath, String fileName)
    {
        File folder = new File(folderPath);
        File[] listFile = folder.listFiles();
        for (File file :listFile)
            if (file.getName().equals(fileName)) return file;
            return null;
    }
    public static void main(String[] args) throws IOException
    {
        String s = readFromFile("src/Tuan9/test.txt");
        System.out.println(s);
        vietFile("src/Tuan9/test.txt");
        vietFile2("src/Tuan9/test.txt");
        File file = findFileByName("src/Tuan1","HelloWorld.java");
        if (file!=null) System.out.println(file.getAbsolutePath());
        else System.out.println("file doesn't exist");
    }
}
